import React, { Component } from "react";

export default class ChangeGlasses extends Component {
  state = {
    imgSrc: "",
  };
  glassChange = (number) => {
    this.setState({
      imgSrc: `../glassesImage/v${number}.png`,
    });
  };

  render() {
    return <div>ChangeGlasses</div>;
  }
}
