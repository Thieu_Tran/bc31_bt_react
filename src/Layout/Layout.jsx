import React, { Component } from "react";
import Item from "../Layout/Item";
import RenderWithMap from "../RenderWithMap/RenderWithMap";
import { glassArr } from "../RenderWithMap/dataGlass";

export default class Layout extends Component {
  state = {
    glassArr: glassArr,
    name: "",
    src: "",
    desc: "",
  };
  changeUrl = (item) => {
    this.setState({
      name: item.name,
      src: item.url,
      desc: item.desc,
    });
  };

  glassRender = () => {
    return this.state.glassArr.map((item) => {
      return (
        <RenderWithMap data={item} handleGlass={this.changeUrl} key={item.id} />
      );
    });
  };

  render() {
    return (
      <div>
        <Item 
        name={this.state.name}
        url={this.state.src}
        desc={this.state.desc} />
        <div className="row">{this.glassRender()}</div>
      </div>
    );
  }
}
