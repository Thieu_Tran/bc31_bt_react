import React, { Component } from "react";
import "./style.css";

export default class Item extends Component {
  render() {
    return (
      <div className="container col-3">
        <div className="card" style={{ width: "100%" }}>
          <img
            className="card-img-top"
            src="./glassesImage/model.jpg"
            alt="Card image cap"
          />
          <div className="d-flex mx-auto" id="glassModel">
            <img
              src={this.props.url}
              style={{ width: "14rem" }}
              alt=""
            />
          </div>
          <div className="card-body text-left">
            <h5 className="card-title">{this.props.name}</h5>
            <p className="card-text">
              {this.props.desc}
            </p>
          </div>
        </div>
      </div>
    );
  }
}
