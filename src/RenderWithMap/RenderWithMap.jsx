import React, { Component } from "react";

export default class RenderWithMap extends Component {
  render() {
    let { name } = this.props.data;
    return (
        <div className="col-2 py-4 px-5 glass__model">
          <img
            onClick={() => {
              this.props.handleGlass(this.props.data);
            }}
            className="img-fluid"
            src={this.props.data.url}
            alt=""
          />
        </div>
    );
  }
}
